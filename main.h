/**
 * SPDX-License-Identifier: Apache-2.0
 * tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)
 * Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Based on:
 * dog (https://github.com/mostsignificant/dog/tree/method-diy)
 * Copyright 2018 Christian Göhring
 * SPDX-License-Identifier: LicenseRef-MIT_dog
 */

/**
 * @file main.h
 * @authors Christian Göhring, DerAndere
 * @copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 * Based on (with modifications): https://github.com/mostsignificant/dog/blob/586359f967507c3b393c6d454c2a1753edab4265/src/main.h
 * Copyright 2018 Christian Göhring
 */

#ifndef MAIN_H
#define MAIN_H

#include <fstream>
#include <iostream>
#include <string>
#include <filesystem>

#include "program_options.h"


std::string file_read(const std::filesystem::path &path);
void file_write(const std::filesystem::path &path, const std::string &content);
std::string convert(const std::string &content);

#endif // MAIN_H
