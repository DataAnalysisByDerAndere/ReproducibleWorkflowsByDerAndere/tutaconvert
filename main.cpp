/** 
 * SPDX-License-Identifier: Apache-2.0
 * tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)
 * Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Based on:
 * dog (https://github.com/mostsignificant/dog/tree/method-diy)
 * Copyright 2018 Christian Göhring
 * SPDX-License-Identifier: LicenseRef-MIT_dog
 */

/**
 * @file main.cpp
 * @authors Christian Göhring, DerAndere
 * @copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
 * Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)
 * Based on (with modifications): https://github.com/mostsignificant/dog/blob/586359f967507c3b393c6d454c2a1753edab4265/src/main.cpp
 * Copyright 2018 Christian Göhring
 */

#include <fstream>
#include <iostream>
#include <string>
#include <filesystem>
#include <stdexcept>
#include <regex>

#include "program_options.h"

#include "main.h"

//#define QUICK_DEBUG

int main(int argc, char *argv[]) {

    std::cout << "########################################################################\n\n";
    std::cout << "tutaconvert is running.\n\n";
    std::cout << "Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)\n";
    std::cout << "Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)\n\n";
    std::cout << "Licensed under the Apache License, Version 2.0 (the \'License\');\n";
    std::cout << "you may not use this file except in compliance with the License.\n";
    std::cout << "You may obtain a copy of the License at\n\n";
    std::cout << "    http://www.apache.org/licenses/LICENSE-2.0.\n\n";
    std::cout << "Unless required by applicable law or agreed to in writing, software\n";
    std::cout << "distributed under the License is distributed on an \'AS IS\' BASIS,\n";
    std::cout << "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n";
    std::cout << "See the License for the specific language governing permissions and\n";
    std::cout << "limitations under the License.\n\n";
    std::cout << "########################################################################\n";
    std::cout << "tutaconvert\n";
    std::cout << "Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)\n";
    std::cout << "Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)\n\n";
    std::cout << "This software includes software developed by DerAndere.\n\n";
    std::cout << "Contributions by DerAndere\n";
    std::cout << "Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues).\n\n";
    std::cout << "This software includes software developed by Christian Göhring.\n\n";
    std::cout << "dog (https://github.com/mostsignificant/dog)\n";
    std::cout << "Copyright 2018 Christian Göhring\n\n";
    std::cout << "This software includes software developed by Joel Bodenmann.\n\n";
    std::cout << "https://blog.insane.engineer/post/cpp_read_file_into_string\n";
    std::cout << "Copyright 2021 Joel Bodenmann\n\n";
    std::cout << "########################################################################\n";
    std::cout << "########################################################################\n";
    std::cout << "########################################################################\n";

    #ifdef QUICK_DEBUG
      std::string test_content = "Previous\nADR;TYPE=home:Bahnhofstrasse 1\\nBerlin\\n1111\\nDeutschland\nRest"; // for testing
      convert(test_content);
    #endif

    try {
        program_options::parse(argc, argv);
    } 
    catch (const std::exception &x) {
        std::cerr << x.what() << '\n';
        std::cerr << "usage: tutaconvert -i <input_file> -o <output_file>\n";
        return EXIT_FAILURE;
    }

    std::filesystem::path input_file = program_options::ifile();

    std::string content = file_read(input_file);

    std::filesystem::path output_file = program_options::ofile();
    file_write(output_file, convert(content));

    return EXIT_SUCCESS;
}


/**
 * https://blog.insane.engineer/post/cpp_read_file_into_string/
 * Copyright 2021 Joel Bodenmann
 * SPDX-License-Identifier: LicenseRef-insane_engineer
 */

/**
 * Returns an std::string which represents the raw bytes of the file.
 *
 * @param path The path to the file.
 * @return The content of the file as it resides on the disk - byte by byte.
 */
std::string file_read(const std::filesystem::path &path)
{
    // Sanity check
    if (!std::filesystem::is_regular_file(path))
        return { };

    // Open the file
    // Note that we have to use binary mode as we want to return a string
    // representing matching the bytes of the file on the file system.
    std::ifstream infile(path, std::ios::in | std::ios::binary);
    if (!infile.is_open())
        return { };

    // Read contents
    const std::size_t &size = std::filesystem::file_size(path);
    std::string content(size, '\0');
    infile.read(content.data(), size);

    // Close the file
    infile.close();

    return content;
}

void file_write(const std::filesystem::path &path, const std::string &content) {
    std::ofstream outfile;
    outfile.open(path);
    outfile << content;
    outfile.close();

}

std::string convert(const std::string &content) {
    std::string processed = content;
    
    // replace multi-line ADR value by single line
    std::regex ex1(R"((ADR(?:;TYPE=\w\w\w\w?\w?\w?\w?)?:.+)\n )");
    processed = std::regex_replace (processed, ex1, "$1");

    // replace up to 6 occurances of \n with ; in lines starting with ADR
    std::regex ex2(R"((ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:.+)\\n)");
    for (int i = 0; i < 6; i++) {
        processed = std::regex_replace (processed, ex2, "$1;");
    }

    // where last address component contains digits, add ; for missing Country
    std::regex ex3 (R"((ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:.+\d[^;]*)(\n[^ ]))");
    processed = std::regex_replace (processed, ex3, "$1;$2");

    // Add ; for missing Region info if only 4 Adress components were specified in the original
    std::regex ex4 (R"((ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:[^;]+;[^;]+;)([^;]+;[^;]*\n[^ ]))");
    processed = std::regex_replace (processed, ex4, "$1;$2");

    // Add ;; at start of ADR value where 5 components are present (assume PO-Box and Extended-Address were not specified)
    std::regex ex5 (R"((ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:)([^;]+;[^;]+;[^;]*;[^;]+;[^;]*\n[^ ]))");
    processed = std::regex_replace (processed, ex5, "$1;;$2");

    // Add ; at start of ADR value where 6 components are present (assume PO-Box was not specified)
    std::regex ex6 (R"((ADR(?:;TYPE=\w\w\w\w\w?\w?\w?\w?)?:)([^;]+;[^;]+;[^;]+;[^;]*;[^;]+;[^;]*\n[^ ]))");
    processed = std::regex_replace (processed, ex6, "$1;$2");

    return processed;
}
