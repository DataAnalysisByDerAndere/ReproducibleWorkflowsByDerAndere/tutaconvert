# TutaConvert
tutaconvert is a tool to convert exported contacts from tutanota into a valid vCard file for migration of address books to other organizer software / Email clients like Thunderbird.

## Installation
1. Download the installer tutaconvert-x.y.z-win64.exe from the release or build and package from the source as described in section "Distribute TutaConvert".
1. Run the installer tutaconvert-x.y.z-win64.exe and follow the instructions on screen.

## Usage
1. In tutanota: Manually format the addresses so that each address has one of the following formats: 
Either
```
Street
Locality
Region
Postal Code
Country
```
or
```
Street
Locality
Postal Code
Country
```

or
```
Street
Locality
Region
Postal Code
```
or
```
Street
Locality
Postal Code
```
Use dummy values (e.g. `0000` for Postal code, `NA` for other address components) as placeholders for missing values of partially existing address information.

2. In tutanota: Click on '...' next to 'All Contacts'
3. In tutanota: Click on 'export vCard'. Enter a file name (e.g. tutanotacontacts) and select the target directory. Click on 'Save'
4. Navigate to the Powershell_ISE.exe and double-click to run the Powershell ISE.
5. In the command prompt, type the following and confirm by pressing the enter-key:

- Method A (using call operator):
```
& tutaconvert --infile "<Path\to\file\tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

- Method B (using direct invocation):
```
<Path\to\parentdirectory\of\tutaconvert.exe>\tutaconvert.exe --infile "<Path\to\file\tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

- Method C (using Set-Location (change directory) and call operator):
```
cd "<Path\to\directory\of\tutanotacontacts.vcf\>"
& tutaconvert --infile "<tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

## Support
https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues

## Contributing
Merge requests (pull requests) targeting the main branch are welcome. For details, see file [CONTRIBUTING.md](./CONRTIBUTING.md).

## Developing TutaConvert
1. Install MSYS2 and update its mingw, including gcc, make and cmake and add edit the System environment variables to add the path to these tools to the System environment variable PATH.
1. Install Microsoft Visual Studio code (VScode)
1. In VScode, install the extension "C/C++ Extension" by Microsoft
1. In VScode, install the extension "CMake tools" by Microsoft
1. In VScode, go to File -\> Preferences -\> Settings. Set the path to those tools. For CMake: Go to submenu Extensions -\> CMake Tools, section "Cmake: Configure Settings" -\> "Edit in settings.json". In that settings.jason, [add the following](https://github.com/microsoft/vscode-cmake-tools/issues/880):
```
    "cmake.cmakePath": "C:\\msys64\\mingw64\\bin\\cmake.exe",
    "cmake.mingwSearchDirs": [
      "C:\\msys64\\mingw64\\bin"
   ],
   "cmake.generator": "MinGW Makefiles"
```
1. Get the source code for tutaconvert
by forking tutaconvert and cloning the fork using git. If you want to download only
the current files of the selected branch, click the control panel "Download"
(next to the control panel "clone").

### Build TutaConvert

In VScode, do the following:
1. Go to View -\> Command Palette. Execute the following commands:
```
CMake: Scan kits
CMake: Select kit
CMake: Configure
CMake: Select Variant -> Debug
CMake: Configure
CMake: Build
CMake: Debug
```
1. Go to "Run and Debug". Left-click the dropdown menu next to "Start Debugging", select "Add config (projectname)". Select "C/C++: (Windows) Launch".
1. open the newly created file launch.json and edit it, if required
1. Go to "Run and Debug" -\> "Start Debugging"

See https://www.youtube.com/watch?v=kEGQKzhciKc&list=PLalVdRk2RC6o5GHu618ARWh0VO0bFlif4&index=2

### Distribute TutaConvert
1. Install NSIS
1. Update all version numbers and push the changes
1. Go to View -\> Command Palette. Execute the following commands:
```
CMake: Select Variant -> Release
CMake: Configure
CMake: Build
```
1. In the terminal which runs the PowerShell, change directory to the subdirectory "build" of the tutaconvert project folder and run cpack:
```
cd build
cpack
``` 
1. Create a tag for the last commit (e.g. tag: 1.0.1, message: "tagged release of tutaconverter v1.0.1" and push the changes
Download https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/-/archive/main/tutaconvert-main.tar.gz and extract the archive.
1. In VScode, Go to View -\> Command Palette. Execute the following commands:
```
CMake: Select Variant -> Release
CMake: Configure
CMake: Build
``` 
1. Log in to gitlab.com
and go to [gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/TutaConvert -\> Deployments -\> Releases](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/-/releases)

Left-click "New Release". Release name: v1.0.1, release notes: tagged release of tutaconvert 1.0.1 and attach the tutaconvert installer (build/_CPack_Packages/win64/NSIS/tutaconvert-x.y.z-win64/tutaconvert-x.y.z-win64.exe).
Left-click "OK"


## Credits
tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)

Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)

Copyright 2022 [DerAndere](https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues) ([Donate](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE))


## License information
```
tutaconvert (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert)
Copyright 2018 - 2022 The TutaConvert Authors (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/blob/main/AUTHORS)
Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/tutaconvert/issues)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
